import random
import string


def generate_login(stringLength=6):  # """Generate a random string of letters, digits"""

    password_characters = string.ascii_letters + string.digits
    return "AutoTEST" + ''.join(random.choice(password_characters) for i in range(stringLength))


def generate_password(stringLength=random.randint(8, 42)):
    """Generate a random string of letters, digits and special characters """

    password_characters = string.ascii_letters + string.digits + string.punctuation
    password_characters.replace(" ", "")
    return "AutoPass" + ''.join(random.choice(password_characters) for i in range(stringLength))


# print("Generating Random String password with letters, digits and special characters ")
# print("First Random String ", generate_password())
# print("Second Random String", generate_password())
# print("Third Random String", generate_password())

def generate_phoneNumber(stringLength=7):
    """Generate a random string of letters, digits and special characters """

    password_characters = string.digits
    return "239" + ''.join(random.choice(password_characters) for i in range(stringLength))
