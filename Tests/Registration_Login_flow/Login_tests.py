import unittest
import datetime
from webbrowser import Chrome

from selenium import webdriver
from PageObjects.RegistrationLoginObjects.LoginPage import LoginPageItems
import HtmlTestRunner
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class RegistrationTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path="D:/B2B_AbInBev/Automation_tests/mytest/Drivers/chromedriver.exe")
        print("Run started at :" + str(datetime.datetime.now()))
        cls.driver.implicitly_wait(5)
        cls.driver.maximize_window()
        cls.driver.get("http://telesaledev.datacenter.ssbs.com.ua/EassistantWeb/auth/login")

    def test_01_verify_page_title(self):
        loginpage = LoginPageItems(self.driver)
        time.sleep(0.1)
        self.assertEqual(self.driver.current_url, loginpage.url)
        self.assertEqual(loginpage.title, self.driver.title)

    def test_02_verify_login_field(self):
        loginpage = LoginPageItems(self.driver)
        loginpage.enter_login("user login")
        loginpage.clear_login_field()
        time.sleep(1)
        self.assertEqual(loginpage.error_message_login, " Обязательное поле. ")
        self.driver.refresh()

    def test_03_verify_password_field(self):
        loginpage = LoginPageItems(self.driver)
        loginpage.enter_password("user login")
        # Verify if password is hidden
        hidden_icon = self.driver.find_element_by_name("password").type = "text"
        self.assertEqual(hidden_icon, "text")

        # Verify if password is shown
        loginpage.eye_verification_for_password()
        # show_icon = self.driver.find_element_by_name("password").type = "text"
        # self.assertEqual(show_icon, "text")
        self.driver.save_screenshot("D:/B2B/AutomatingTesting_B2B/Screenshots/eye_is_open.png")
        loginpage.eye_verification_for_password()
        self.driver.save_screenshot("D:/B2B/AutomatingTesting_B2B/Screenshots/eye_is_closed.png")


        # Verification for error message if field is empty
        loginpage.clear_password_field()
        self.assertEqual(loginpage.error_message_login, " Обязательное поле. ")

    def test_04_verify_password_eye_button(self):
        pass

    def test_05_verify_forgot_password_or_login(self):
        pass

    def test_05_verify_login_button(self):
        pass

    def test_06_verify_register_button(self):
        pass

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("Finished at :" + str(datetime.datetime.now()))


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output="D:/B2B_AbInBev/Automation_tests/mytest/reports"))
