import unittest
import datetime
from selenium import webdriver
from PageObjects.RegistrationLoginObjects.LoginPage import LoginPageItems
import HtmlTestRunner
import time


class PasswordRecoveryTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path="D:\B2B\AutomatingTesting_B2B\Drivers\chromedriver.exe")
        print("Run started at :" + str(datetime.datetime.now()))
        cls.driver.implicitly_wait(5)
        cls.driver.maximize_window()
        cls.driver.get("http://b2bdev.datacenter.ssbs.com.ua/B2BDemoWeb")

    def test_01_verify_page_title(self):
        loginpage = LoginPageItems(self.driver)
        time.sleep(0.1)
        self.assertEqual(self.driver.current_url, loginpage.url)
        self.assertEqual(self.driver.title, loginpage.title)

    def test_02_verify_login_field(self):
        loginpage = LoginPageItems(self.driver)
        loginpage.enter_login("user login")
        self.assertEqual(loginpage.error_message_login1, self.driver.f)

    def test_03_verify_password_field(self):
        pass

    def test_04_verify_password_eye_button(self):
        pass

    def test_05_verify_forgot_password_or_login(self):
        pass

    def test_05_verify_login_button(self):
        pass

    def test_06_verify_register_button(self):
        pass

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("Finished at :" + str(datetime.datetime.now()))


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output="D:/B2B/AutomatingTesting_B2B/reports"))
