import unittest
import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains

from Tests.Autogenerator.Generators import *

from PageObjects.RegistrationLoginObjects.RegistrationPage import RegistrationPageItems, RegistrationPageErrors
from PageObjects.RegistrationLoginObjects.RegistrationPage import CodeVerificationPageItems
from PageObjects.RegistrationLoginObjects.LoginPage import LoginPageItems
import HtmlTestRunner
import time
from DataBase.db import db_actions

# Global variables for random generated login and password
random_login = generate_login()
random_password = generate_password()
random_phoneNubmer = generate_phoneNumber()

webPhoneNumber = "9315436203"

home = "D:/B2B_AbInBev/Automation_tests/mytest/Drivers/chromedriver.exe"
work = "D:/B2B/AutomatingTesting_B2B/mytest/Drivers/chromedriver.exe"


class RegistrationTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('prefs', {'intl.accept_languages': 'ru'})
        cls.driver = webdriver.Chrome(executable_path=work,
                                      options=options)
        print("Run started at :" + str(datetime.datetime.now()))
        cls.driver.implicitly_wait(5)
        cls.driver.maximize_window()
        cls.driver.get("http://telesaledev.datacenter.ssbs.com.ua/EassistantWeb/auth/login")
        cls.driver.implicitly_wait(10)
        time.sleep(1)

    """Валідація полів на сторінці 'Регистрация' """

    def test_01_verify_page(self):
        loginpage = LoginPageItems(self.driver)
        loginpage.click_on_register_button_()
        time.sleep(0.7)
        self.driver.implicitly_wait(10)
        registrationpage = RegistrationPageItems(self.driver)

        with self.subTest("Перевірка URL"):
            self.assertEqual(self.driver.current_url, registrationpage.url)

        with self.subTest("Перевірка заголовку сторінки"):
            self.assertEqual(self.driver.title, registrationpage.title)

        with self.subTest("Перевірка назви сторінки"):
            self.assertEqual(self.driver.find_element_by_tag_name("h2").text, registrationpage.pagename)

    def test_02_verify_login_field(self):
        registrationpage = RegistrationPageItems(self.driver)
        errors = RegistrationPageErrors(self.driver)

        registrationpage.enter_login("12")
        with self.subTest("Перевірка введення двох символів"):
            self.assertEqual(errors.get_error_1(), "Не менее 3-х символов.")

        registrationpage.enter_login("TT ")
        with self.subTest("Перевірка введення пробілу"):
            self.assertEqual(errors.get_error_2(),
                             "Цифры и\или латинские буквы без пробелов.")

        registrationpage.enter_login("ТТФілька")
        with self.subTest("Перевірка введення кирилиці"):
            self.assertEqual(errors.get_error_2(),
                             "Цифры и\или латинские буквы без пробелов.")

        registrationpage.enter_login("*.,/-+**/~12")
        with self.subTest("Перевірка введення спеціальних символів"):
            self.assertEqual(errors.get_error_2(),
                             "Цифры и\или латинские буквы без пробелов.")

        registrationpage.enter_login("Т ")
        with self.subTest("Перевірка введення менше 3-ьох символів і пробілу"):
            self.assertEqual(errors.get_error_2(),
                             "Не менее 3-х символов. Цифры и\или латинские буквы без пробелов.")

        registrationpage.enter_login("1ф")
        with self.subTest("Перевірка введення вдох символів (одна цифра і одна буква кирилиці)"):
            self.assertEqual(errors.get_error_2(),
                             "Не менее 3-х символов. Цифры и\или латинские буквы без пробелов.")

        registrationpage.clear_login_field()
        with self.subTest("Перевірка пустого поля"):
            self.assertEqual(errors.get_error_2(),
                             "Обязательное поле.")

        registrationpage.enter_login(random_login)

    def test_03_verify_mobile_phone_field(self):
        registrationpage = RegistrationPageItems(self.driver)
        errors = RegistrationPageErrors(self.driver)

        registrationpage.enter_phoneNumber("asd")
        with self.subTest("Перевірка введення латинських букв"):
            self.assertEqual(errors.get_error_4(),
                             "Некорректный номер телефона")

        registrationpage.enter_phoneNumber("фіфі")
        with self.subTest("Перевірка введення букв кирилиці"):
            self.assertEqual(errors.get_error_4(),
                             "Некорректный номер телефона")

        registrationpage.enter_phoneNumber("+-!'№;%")
        with self.subTest("Перевірка введення спец символів"):
            self.assertEqual(errors.get_error_4(),
                             "Некорректный номер телефона")

        registrationpage.enter_phoneNumber("123412349")
        with self.subTest("Перевірка введення 9 цифр"):
            self.assertEqual(errors.get_error_4(),
                             "Некорректный номер телефона")

        registrationpage.clear_phoneNumber_field()
        with self.subTest("Перевірка пустого поля"):
            self.assertEqual(errors.get_error_2(),
                             "Обязательное поле.")

        registrationpage.enter_phoneNumber(random_phoneNubmer)
        # with self.subTest("Перевірка введення вірного номера телефона"):
        # self.assertEqual(errors.check_exists_error_4(), True)

    def test_04_verify_password_field(self):
        registrationpage = RegistrationPageItems(self.driver)
        errors = RegistrationPageErrors(self.driver)

        registrationpage.enter_password("zxzzz32")
        with self.subTest("Перевірка введення необхідної кількості символів"):
            self.assertEqual(errors.get_error_5(),
                             "Не менее 8-ми символов.")

        registrationpage.enter_password("asda asdaa")
        with self.subTest("Перевірка введення пробілу і більше 8 символів"):
            self.assertEqual(errors.get_error_2(),
                             "Пароль не должен содержать пробелы.")

        registrationpage.enter_password("vasa NN")
        with self.subTest("Перевірка введення пробілу і менше 8 символів"):
            self.assertEqual(errors.get_error_2(),
                             "Не менее 8-ми символов. Пароль не должен содержать пробелы.")

        registrationpage.enter_password("1234567890")
        with self.subTest("Перевірка введення тільки цифр"):
            self.assertEqual(errors.get_error_6(),
                             "Пароль должен быть не менее 8 символов, содержать цифры и заглавные буквы.")

        registrationpage.enter_password("tamadatamadatamada")
        with self.subTest("Перевірка введення тільки латинських букв"):
            self.assertEqual(errors.get_error_6(),
                             "Пароль должен быть не менее 8 символов, содержать цифры и заглавные буквы.")

        registrationpage.enter_password("тамадатамадатамадатамада")
        with self.subTest("Перевірка введення тільки букв кирилиці"):
            self.assertEqual(errors.get_error_6(),
                             "Пароль должен быть не менее 8 символов, содержать цифры и заглавные буквы.")

        registrationpage.enter_password("!@#$%^&*()_-=+'/|")
        with self.subTest("Перевірка введення тільки спец. символів"):
            self.assertEqual(errors.get_error_6(),
                             "Пароль должен быть не менее 8 символов, содержать цифры и заглавные буквы.")

        registrationpage.enter_password("tamada007tamada007tamada007")
        with self.subTest("Перевірка введення цифр і латинських букв в нижньому регістрі"):
            self.assertEqual(errors.get_error_6(),
                             "Пароль должен быть не менее 8 символов, содержать цифры и заглавные буквы.")

        registrationpage.enter_password("тамада007тамада007тамада007")
        with self.subTest("Перевірка введення цифр і букв кирилиці в нижньому регістрі"):
            self.assertEqual(errors.get_error_6(),
                             "Пароль должен быть не менее 8 символов, содержать цифры и заглавные буквы.")

        registrationpage.enter_password("тамада007tamada007")
        with self.subTest("Перевірка введення цифр і букв кирилиці і латинські букви в нижньому регістрі"):
            self.assertEqual(errors.get_error_6(),
                             "Пароль должен быть не менее 8 символов, содержать цифры и заглавные буквы.")

        registrationpage.enter_password("тамада007tamada007#@!)")
        with self.subTest("Перевірка введення цифр+букви кирилиці+латинські букви+спец. символи+нижній регістр"):
            self.assertEqual(errors.get_error_6(),
                             "Пароль должен быть не менее 8 символов, содержать цифры и заглавные буквы.")

        registrationpage.enter_password("TAMADA007ТАМАДА")
        with self.subTest("Перевірка введення цифр+латинські букви+кирилиця+верхній регістр"):
            self.assertEqual(errors.get_error_6(),
                             "Пароль должен быть не менее 8 символов, содержать цифры и заглавные буквы.")

        registrationpage.clear_password_field()
        with self.subTest("Перевірка пустого поля"):
            self.assertEqual(errors.get_error_2(),
                             "Обязательное поле.")

        registrationpage.enter_password(random_password)

    def test_05_verify_password_eye_button(self):
        registrationpage = RegistrationPageItems(self.driver)
        registrationpage.click_on_password_eye()
        self.driver.save_screenshot("D:/B2B_AbInBev/Automation_tests/mytest/Screenshots/eye_is_open.png")

        with self.subTest("Перевірка кнопки показати пароль - пароль відкритий"):
            self.assertEqual(registrationpage.state_of_eye_button_1(), "icon-font ng-star-inserted icon-show_password")

        registrationpage.state_of_eye_button_1()
        registrationpage.click_on_password_eye()
        self.driver.save_screenshot("D:/B2B_AbInBev/Automation_tests/mytest/Screenshots/eye_is_closed.png")

        with self.subTest("Перевірка кнопки сховати пароль - пароль закритий"):
            self.assertEqual(registrationpage.state_of_eye_button_1(), "icon-font ng-star-inserted icon-hide_password")

    def test_06_verify_passwordConfirmation_field(self):
        registrationpage = RegistrationPageItems(self.driver)
        errors = RegistrationPageErrors(self.driver)

        registrationpage.enter_passwordConfirmation("x1y2")
        with self.subTest("Перевірка співпадіння паролів"):
            self.assertEqual(errors.get_error_7(),
                             "Пароли не совпадают.")

        registrationpage.clear_passwordConfirmation_field()
        with self.subTest("Перевірка пустого поля"):
            self.assertEqual(errors.get_error_2(),
                             "Обязательное поле. Пароли не совпадают.")

        registrationpage.enter_passwordConfirmation(random_password)

    def test_07_verify_passwordConfirmation_eye_button(self):
        registrationpage = RegistrationPageItems(self.driver)
        registrationpage.click_on_passwordConfirmation_eye()
        self.driver.save_screenshot("D:/B2B_AbInBev/Automation_tests/mytest/Screenshots/eye2_is_open.png")
        with self.subTest("Перевірка кнопки показати пароль підтвердження - пароль відкритий"):
            self.assertEqual(registrationpage.state_of_eye_button_2(), "icon-font ng-star-inserted icon-show_password")

        registrationpage.click_on_passwordConfirmation_eye()
        self.driver.save_screenshot("D:/B2B_AbInBev/Automation_tests/mytest/Screenshots/eye2_is_closed.png")
        with self.subTest("Перевірка кнопки сховати пароль підтвердження- пароль закритий"):
            self.assertEqual(registrationpage.state_of_eye_button_2(), "icon-font ng-star-inserted icon-hide_password")

    def test_08_verify_checkbox_isAgreedLicenseAgreement(self):
        registrationpage = RegistrationPageItems(self.driver)
        registrationpage.click_on_isAgreedLicenseAgreement()

        with self.subTest("Перевірка назви сторінки згода на обробку даних"):
            self.assertEqual(registrationpage.license_page_title(),
                             "Согласие на обработку персональных данных")

        with self.subTest("Перевірка URL сторінки згода на обробку даних"):
            self.assertEqual(registrationpage.licensepageURL, self.driver.current_url)
        registrationpage.close_license_page()

    def test_09_verify_login_button(self):
        registrationpage = RegistrationPageItems(self.driver)
        loginpage = LoginPageItems(self.driver)
        registrationpage.click_on_login_button()
        with self.subTest("Перевірка перенапрвалення на сторінку Логування"):
            self.assertEqual(self.driver.find_element_by_tag_name("h2").text, loginpage.pagename)

        loginpage.click_on_register_button_()

    def test_10_verify_next_button(self):
        registrationpage = RegistrationPageItems(self.driver)
        with self.subTest("Перевірка кнопка далее неактивна"):
            self.assertEqual(registrationpage.state_of_next_button(), "true")

    def test_11_registration_with_valid_data(self):
        registrationpage = RegistrationPageItems(self.driver)
        registrationpage.enter_login(random_login)
        registrationpage.enter_phoneNumber(webPhoneNumber)
        registrationpage.enter_password(random_password)
        registrationpage.enter_passwordConfirmation(random_password)
        registrationpage.click_on_checkbox()

        with self.subTest("Перевірка кнопка далее неактивна"):
            self.assertEqual(registrationpage.state_of_next_button(), "false")

        registrationpage.click_on_next_button()
        time.sleep(1)

    """Перевірка сторінки 'Коду підтвердження коду реєстрації' """

    def test_12_code_verification_page(self):
        codepage = CodeVerificationPageItems(self.driver)
        registrationpage = RegistrationPageItems(self.driver)

        with self.subTest("Перевірка кнопка далее неактивна"):
            self.assertEqual(codepage.state_of_next_button(), "true")

        with self.subTest("Перевірка тексту повідомлення на ваш номер..."):
            self.assertEqual(codepage.get_info_message(), "На Ваш номер")

        with self.subTest("Перевірка назви сторінки"):
            self.assertEqual(codepage.get_page_title(), "Подтверждение регистрации")

        codepage.enter_code_verification("1XXX1")
        codepage.click_on_next_button()
        with self.subTest("Перевірка введенян невірного коду"):
            self.assertEqual(codepage.get_error_1(), "Введен неверный код. Повторите попытку")

        codepage.click_on_nocode_press_here_button()
        time.sleep(0.5)
        with self.subTest("Перевірка назви сторінки"):
            self.assertEqual(self.driver.find_element_by_tag_name("h2").text, registrationpage.pagename)

        registrationpage.click_on_next_button()
        time.sleep(0.5)
        with self.subTest("Перевірка назви сторінки"):
            self.assertEqual(codepage.get_page_title(), "Подтверждение регистрации")

        codepage.click_on_next_button()

    def test_13_verify_user_registration(self):
        codepage = CodeVerificationPageItems(self.driver)
        registartionpage = RegistrationPageItems(self.driver)
        time.sleep(12)
        cd = codepage.get_verification_code()
        codepage.enter_code_verification(cd)
        codepage.click_on_next_button()
        time.sleep(2)
        with self.subTest("Перевірка реєстарції користувача після введення вірного коду підтвердження реєстрації"):
            self.assertEqual(self.driver.current_url, registartionpage.catalogURL)

    """Перевірка запису відповідних полів в БД після створення користувача"""

    def test_14_verify_DBfields(self):
        app_users_fields = str([(1, str(random_login), str(webPhoneNumber), 2, True)])
        OLCred_fields = str([(str(random_login), str(random_password), True)])

        with self.subTest("Перевірка запису потрібних полів в БД tbl.ApplicationUsers"):
            self.assertEqual(app_users_fields, str(db_actions.select_from_ApplicationUsers()))

        with self.subTest("Перевірка запису потрібних полів в БД tbl.OLCred"):
            self.assertEqual(OLCred_fields, str(db_actions.select_from_OLCred()))

    def test_15_verify_registration_of_registered_user(self):
        registrationpage = RegistrationPageItems(self.driver)
        errors = RegistrationPageErrors(self.driver)


        with self.subTest("Перевірка введення логіну, який вже є записаний в БД для активного користувача"):
            self.assertEqual(errors.get_error_8(),
                             "Пользователь с указанным логином уже зарегистрирован. "
                             "Пожалуйста войдите в систему как зарегистрированный пользователь.")

        with self.subTest("Перевірка введення телефону, який вже є записаний в БД для активного користувача"):
            self.assertEqual(errors.get_error_8(),
                             "Пользователь с указанным телефоном уже зарегистрирован. "
                             "Пожалуйста войдите в систему как зарегистрированный пользователь ")

        with self.subTest("Перевірка введення логіну та телефону, який вже є записаний в БД для активного користувача"):
            self.assertEqual(errors.get_error_8(),
                             "Пользователь с указанным логином и телефоном уже зарегистрирован. "
                             "Пожалуйста войдите в систему как зарегистрированный пользователь ")

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("Finished at :" + str(datetime.datetime.now()))


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output="D:/B2B/AutomatingTesting_B2B/reports"))
