import time

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class RegistrationPageItems:
    def __init__(self, driver):
        self.driver = driver

        # Locators of available options

        self.url = "http://telesaledev.datacenter.ssbs.com.ua/EassistantWeb/auth/outlet"
        self.licensepageURL = "http://telesaledev.datacenter.ssbs.com.ua/EassistantWeb/auth/outlet/license?type=license"
        self.catalogURL = "http://telesaledev.datacenter.ssbs.com.ua/EassistantWeb/catalog"

        self.title = "E-assistant"

        self.pagename = "Регистрация"

        self.login_field_by_name = "login"

        self.phoneNumber_field_by_name = "phoneNumber"

        self.password_field_by_name = "password"
        self.eye_for_password_field_by_xpath = "/html[1]/body[1]/app-root[1]/app-auth-layout[1]/section[1]/div[2]/" \
                                               "div[1]/div[1]/div[1]/div[1]/app-outlet[1]/form[1]/div[1]/" \
                                               "app-field-text-input[2]/mat-form-field[1]/div[1]/div[1]/div[2]/i[1]"

        self.passwordConfirmation_field_by_name = "passwordConfirmation"
        self.eye_for_password_confirmation_field_by_xpath = "/html[1]/body[1]/app-root[1]/app-auth-layout[1]/" \
                                                            "section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/" \
                                                            "app-outlet[1]/form[1]/div[1]/" \
                                                            "app-field-text-input[3]/mat-form-field[1]/div[1]/" \
                                                            "div[1]/div[2]/i[1]"

        self.click_checkbox_isAgreed_by_xpath = "//div[@class='mat-checkbox-inner-container']"
        self.click_on_isAgreedLicenseAgreement_by_xpath = "//a[@class='service-link']"

        # self.next_button_by_xpath = "//button[@class='full-width auth-btn text-uppercase mat-raised-button mat-accent']"
        self.next_button_by_tag_name = "button"
        self.login_button_by_xpath = "/html[1]/body[1]/app-root[1]/app-auth-layout[1]/section[1]/div[2]/div[1]/" \
                                     "div[1]/div[1]/div[1]/app-outlet[1]/form[1]/div[2]/a[1]"

    # Registration page
    def enter_login(self, user_login):
        self.driver.find_element_by_name(self.login_field_by_name).clear()
        self.driver.find_element_by_name(self.login_field_by_name).send_keys(user_login)

    def clear_login_field(self):
        self.driver.find_element_by_name(self.login_field_by_name).send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_name(self.login_field_by_name).send_keys(Keys.DELETE)

    def enter_phoneNumber(self, user_phone):
        self.driver.find_element_by_name(self.phoneNumber_field_by_name).clear()
        self.driver.find_element_by_name(self.phoneNumber_field_by_name).send_keys(user_phone)

    def clear_phoneNumber_field(self):
        self.driver.find_element_by_name(self.phoneNumber_field_by_name).send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_name(self.phoneNumber_field_by_name).send_keys(Keys.DELETE)

    #
    def enter_password(self, user_pw):
        self.driver.find_element_by_name(self.password_field_by_name).clear()
        self.driver.find_element_by_name(self.password_field_by_name).send_keys(user_pw)

    def clear_password_field(self):
        self.driver.find_element_by_name(self.password_field_by_name).send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_name(self.password_field_by_name).send_keys(Keys.DELETE)

    def click_on_password_eye(self):
        WebDriverWait(self.driver, 0).until(
            ec.visibility_of_element_located((By.XPATH, self.eye_for_password_field_by_xpath))).click()

    def state_of_eye_button_1(self):
        attribute_value = WebDriverWait(self.driver, 0).until(ec.visibility_of_element_located((
            By.XPATH, self.eye_for_password_field_by_xpath))).get_attribute("class")
        return attribute_value

    ###

    #
    def enter_passwordConfirmation(self, user_conf_pw):
        self.driver.find_element_by_name(self.passwordConfirmation_field_by_name).clear()
        self.driver.find_element_by_name(self.passwordConfirmation_field_by_name).send_keys(user_conf_pw)

    def clear_passwordConfirmation_field(self):
        self.driver.find_element_by_name(self.passwordConfirmation_field_by_name).send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_name(self.passwordConfirmation_field_by_name).send_keys(Keys.DELETE)

    def click_on_passwordConfirmation_eye(self):
        WebDriverWait(self.driver, 0).until(
            ec.visibility_of_element_located(
                (By.XPATH, self.eye_for_password_confirmation_field_by_xpath))).click()

    def state_of_eye_button_2(self):
        attribute_value = WebDriverWait(self.driver, 0).until(ec.visibility_of_element_located((
            By.XPATH, self.eye_for_password_confirmation_field_by_xpath))).get_attribute("class")
        return attribute_value

    ###

    def click_on_checkbox(self):
        WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, self.click_checkbox_isAgreed_by_xpath))).click()

    def click_on_isAgreedLicenseAgreement(self):
        WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, self.click_on_isAgreedLicenseAgreement_by_xpath))).click()

    def click_on_next_button(self):
        WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.TAG_NAME, self.next_button_by_tag_name))).click()

    def state_of_next_button(self):
        attribute_value = WebDriverWait(self.driver, 2).until(ec.visibility_of_element_located((
            By.TAG_NAME, self.next_button_by_tag_name))).get_attribute("ng-reflect-disabled")
        return attribute_value

    def click_on_login_button(self):  # login as registered user
        WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, self.login_button_by_xpath))).click()

    # License Page Items
    def license_page_title(self):
        txt = self.driver.find_element_by_xpath("//div[@class='license-page']//div//span")
        return txt.text

    def close_license_page(self):
        self.driver.find_element_by_xpath("//i[@class='material-icons']//img").click()
    ###

# Error messages
class RegistrationPageErrors:
    def __init__(self, driver):
        self.driver = driver

    """Login field"""

    # Error 1 = "Не менее 3-х символов."
    # Error 2 = "Цифры и\или латинские буквы без пробелов."
    # Error 2 = "Не менее 3-х символов. Цифры и\или латинские буквы без пробелов."
    # Error 2 = "Обязательное поле."
    # Error 3 = "Коритсувач вже зареєстрований"

    def get_error_1(self):  # Error 1 "Не менее 3-х символов."
        error_txt = WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, "//app-mat-error[contains(text(),'3-')]")))
        return error_txt.text

    def get_error_2(self):  # Error 1,2,3
        error_txt = WebDriverWait(self.driver, 3).until(
            ec.visibility_of_element_located((By.XPATH, "//app-mat-error[contains(text(),'.')]")))
        return error_txt.text

    def get_error_3(self):
        error_txt = WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, "")))
        return error_txt.text

    """Mobile field"""

    # Error 2 = "Обязательное поле."
    # Error 4 = "Некорректный номер телефона"
    def get_error_4(self):
        error_txt = WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, "/html[1]/body[1]/app-root[1]/app-auth-layout[1]/section[1]/"
                                                        "div[2]/div[1]/div[1]/div[1]/div[1]/app-outlet[1]/form[1]/"
                                                        "div[1]/div[1]/app-field-phone-input[1]/mat-form-field[1]/"
                                                        "div[1]/div[3]/div[1]/mat-error[1]/app-mat-error[1]")))
        return error_txt.text

    def check_exists_error_4(self):
        try:
            WebDriverWait(self.driver, 0).until(
                ec.visibility_of_element_located(
                    (By.XPATH, "/html[1]/body[1]/app-root[1]/app-auth-layout[1]/section[1]/"
                               "div[2]/div[1]/div[1]/div[1]/div[1]/app-outlet[1]/form[1]/"
                               "div[1]/div[1]/app-field-phone-input[1]/mat-form-field[1]/"
                               "div[1]/div[3]/div[1]/mat-error[1]/app-mat-error[1]")))
        except (NoSuchElementException, TimeoutException):
            return True
        return False

    """Password field"""

    # Error 2 = "Обязательное поле."
    # Error 5 = "Не менее 8-ми символов."
    # Error 2 = "Пароль не должен содержать пробелы."
    # Error 2 = "Не менее 8-ми символов. Пароль не должен содержать пробелы."
    # Error 6 = "Пароль должен быть не менее 8 символов, содержать цифры и заглавные буквы."
    def get_error_5(self):
        error_txt = WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, "//app-mat-error[contains(text(),'8-')]")))
        return error_txt.text

    def get_error_6(self):
        error_txt = WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, "//app-mat-error[contains(text(),'8')]")))
        return error_txt.text


    """Password Confirmation filed"""

    # Error 2 = "Обязательное поле."
    # Error 7 = "Значение поля Новый пароль и Подтверждение пароля не совпадают."
    def get_error_7(self):
        error_txt = WebDriverWait(
            self.driver, 2).until(ec.visibility_of_element_located((
            By.XPATH, "/html[1]/body[1]/app-root[1]/app-auth-layout[1]/section[1]/div[2]/div[1]/div[1]/div[1]/"
                      "div[1]/app-outlet[1]/form[1]/div[1]/app-field-text-input[3]/mat-form-field[1]/div[1]/"
                      "div[3]/div[1]/mat-error[1]/app-mat-error[1]")))
        return error_txt.text


    """Registered user (Already exist login / phone number / )"""
    def get_error_8(self):
        # error_txt = WebDriverWait(self.driver, 2).until(
        #     ec.visibility_of_element_located((By.XPATH, "")))
        # return error_txt.text
        pass



class CodeVerificationPageItems:  # Code verification page
    def __init__(self, driver):
        self.driver = driver

        # Elements
        self.login_field_by_name = "login"  # verification code
        self.next_button_by_xpath = "//button[@class='full-width auth-btn text-uppercase mat-raised-button mat-accent']"
        self.url = "http://telesaledev.datacenter.ssbs.com.ua/EassistantWeb/auth/confirm?confirm=reg"

    def get_verification_code(self):
        self.driver.execute_script(
            '''window.open("https://receive-sms.cc/Russia-Phone-Number/79315436203", "_blank");''')
        self.driver.switch_to.window(self.driver.window_handles[1])
        time.sleep(0.5)  # wait for sms time
        code = self.driver.find_element_by_class_name('btn1').text
        self.driver.close()
        self.driver.switch_to.window(self.driver.window_handles[0])
        return code

    def enter_code_verification(self, ver_code):
        self.driver.find_element_by_name("code").clear()
        self.driver.find_element_by_name("code").send_keys(ver_code)

    def get_page_title(self):
        txt = self.driver.find_element_by_tag_name("h2").text
        return txt

    def get_info_message(self):
        txt = self.driver.find_element_by_xpath("//div[@class='note note-send-code']//span[1]").text
        return txt

    def click_on_next_button(self):
        WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.TAG_NAME, "button"))).click()

    def state_of_next_button(self):
        attribute_value = WebDriverWait(self.driver, 2).until(ec.visibility_of_element_located((
            By.TAG_NAME, "button"))).get_attribute("ng-reflect-disabled")
        return attribute_value

    def click_on_nocode_press_here_button(self):
        WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, "//a[@class='service-link bold']"))).click()


    """Errors"""

    def get_error_1(self):
        error_txt = WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((
                By.XPATH, "/html[1]/body[1]/app-root[1]/app-auth-layout[1]/section[1]/div[2]/div[1]/div[1]/div[1]/"
                          "div[1]/app-recover-code[1]/form[1]/app-error-message[1]/div[1]")))
        return error_txt.text
