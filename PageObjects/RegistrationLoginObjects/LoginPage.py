from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class LoginPageItems:
    def __init__(self, driver):
        self.driver = driver

        self.url = "http://telesaledev.datacenter.ssbs.com.ua/EassistantWeb/auth/login"
        self.title = "E-assistant"
        self.pagename = "Вход"
        self.login_field_by_name = "login"
        self.password_field_by_name = "password"
        self.eye_icon_for_password_field_by_tag_name = "i"
        self.forgot_password_or_login_by_xpath = "//a[@class='service-link bold']"
        self.login_button_by_xpath = "//button[@class='full-width b2b-button mat-flat-button mat-accent']"
        self.register_button_by_xpath = "//a[@class='full-width b2b-button link-button mat-flat-button']"

        # error messages
        self.error_message_login = " Обязательное поле. "
        self.error_message_password = "//mat-error[@id='mat-error-1']"
        self.error_message_login3 = ""
        self.error_message_login4 = ""

    # Available methods
    def url_verification(self):
        pass

    def enter_login(self, user_login):
        self.driver.find_element_by_name(self.login_field_by_name).clear()
        self.driver.find_element_by_name(self.login_field_by_name).send_keys(user_login)

    def clear_login_field(self):
        self.driver.find_element_by_name(self.login_field_by_name).send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_name(self.login_field_by_name).send_keys(Keys.DELETE)

    def enter_password(self, user_password):
        self.driver.find_element_by_name(self.password_field_by_name).clear()
        self.driver.find_element_by_name(self.password_field_by_name).send_keys(user_password)

    def clear_password_field(self):
        self.driver.find_element_by_name(self.password_field_by_name).send_keys(Keys.CONTROL + "a")
        self.driver.find_element_by_name(self.password_field_by_name).send_keys(Keys.DELETE)

    def eye_verification_for_password(self):
        WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, self.eye_icon_for_password_field_by_tag_name))).click()

    def click_on_forgot_password_or_login(self):
        WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, self.forgot_password_or_login_by_xpath))).click()

    def click_on_login_button(self):
        WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, self.login_button_by_xpath))).click()

    def click_on_register_button_(self):
        WebDriverWait(self.driver, 2).until(
            ec.visibility_of_element_located((By.XPATH, self.register_button_by_xpath))).click()

    # Error messages methods


    def find_error_message1(self):
        msg = self.driver.find_element_by_link_text(self.error_message_login).text
        return msg
