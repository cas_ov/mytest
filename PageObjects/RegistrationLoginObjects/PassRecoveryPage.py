class PassRecoveryPageItems:
    def __init__(self, driver):
        self.driver = driver

        self.url = "http://b2bdev.datacenter.ssbs.com.ua/B2BDemoWeb/auth/login"
        self.title = "E-assistant"

        # Password recovery Page elements
        self.login_field_by_name = "login"
        self.forgot_login_by_class_name = "forgot_login"
        self.next_button_by_class_name = ""

        # Code verification page elements
        self.code_for_number_verification = ""
        self.no_code_press_here = ""
        self.next_button_by_class_name = ""

        # Entering login, password and password confirmation page
        self.login_field_by_name = "login"
        self.password_field_by_name = "password"
        self.eye_icon_for_password_field_by_xpath = ""
        self.eye_icon_for_password_confirmation_field_by_xpath = ""
        self.login_button_by_class_name = ""

        # error messages
        self.error_message_login1 = ""
        self.error_message_login2 = ""
        self.error_message_login3 = ""
        self.error_message_login4 = ""

    # Available methods enter login page
    def url_verification(self):
        pass

    def enter_login(self, user_login):
        self.driver.find_element_by_name(self.login_field_by_name).clear()
        self.driver.find_element_by_name(self.login_field_by_name).send_keys(user_login)

    def click_on_forgot_login_button(self):
        pass

    def click_on_next_button(self):
        self.driver.find_element_by_xpath(self.login_button_by_class_name).click()

    # Available methods Code verification page




    def enter_password(self, user_password):
        self.driver.find_element_by_name(self.password_field_by_name).clear()
        self.driver.find_element_by_name(self.password_field_by_name).send_keys(user_password)

    def eye_verification_for_password(self):
        self.driver.find_element_by_xpath(self.eye_icon_for_password_field_by_xpath).click()

    def click_on_forgot_password_or_login(self):
        self.driver.find_element_by_class_name(self.forgot_password_or_login_by_class_name).click()

    def click_on_login_button(self):
        self.driver.find_element_by_xpath(self.login_button_by_class_name).click()


    # Error messages methods

    def find_error_message1(self):
        pass
